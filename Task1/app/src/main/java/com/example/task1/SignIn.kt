package com.example.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class SignIn : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signin)
        supportActionBar?.show()
        val page = intent.getStringExtra("page")
        title = page
    }
    fun mainPage(v: View) {
        val page = (v as Button).text
        val intent = Intent(this@SignIn, MainPage::class.java)
        intent.putExtra("page",page)
        startActivity(intent)
    }
}
