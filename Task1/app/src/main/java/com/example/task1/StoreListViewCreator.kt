package com.example.task1

import android.util.Log
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView


import com.example.task1.MainPageList.OnListFragmentInteractionListener
import com.example.task1.order.StoreList

import kotlinx.android.synthetic.main.fragment_item.view.*

/**
 * [RecyclerView.Adapter] that can display a [DummyItem] and makes a call to the
 * specified [OnListFragmentInteractionListener].
 * TODO: Replace the implementation with code for your data type.
 */
class StoreListViewCreator(
    private val mStoreImage: List<StoreList.StoreImage>,
    private val mStoreName: List<StoreList.StoreName>,
    private val mStoreType: List<StoreList.StoreType>,
    private val mListener: OnListFragmentInteractionListener?
) : RecyclerView.Adapter<StoreListViewCreator.ViewHolder>() {

    private val mOnClickListener: View.OnClickListener

    init {
        mOnClickListener = View.OnClickListener { v ->
            val item = v.tag as StoreList.StoreImage
            // Notify the active callbacks interface (the activity, if the fragment is attached to
            // one) that an item has been selected.
            mListener?.onListFragmentInteraction(item)
            Log.d("mListener", mListener.toString())
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.fragment_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val storeImage = mStoreImage[position]
        val storeName = mStoreName[position]
        val storeType = mStoreType[position]
        holder.mStoreImageView.setImageResource(storeImage.storeImage)
        holder.mStoreNameView.text = storeName.storeName
        holder.mStoreTypeView.text = storeType.storeType

        with(holder.mView) {
            tag = storeImage
            setOnClickListener(mOnClickListener)
        }
    }

    override fun getItemCount(): Int = mStoreImage.size

    inner class ViewHolder(val mView: View) : RecyclerView.ViewHolder(mView) {
        val mStoreImageView: ImageView = mView.item_image
        val mStoreNameView: TextView = mView.store_name
        val mStoreTypeView : TextView = mView.store_type

        override fun toString(): String {
            return super.toString() + " '" + mStoreNameView.text + "'"
        }
    }
}
