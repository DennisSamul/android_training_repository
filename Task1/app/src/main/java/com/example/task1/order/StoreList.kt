package com.example.task1.order

import com.example.task1.R
import java.util.ArrayList
import java.util.HashMap

/**
 * Helper class for providing sample store_name for user interfaces created by
 * Android template wizards.
 *
 * TODO: Replace all uses of this class before publishing your app.
 */
object StoreList {
    private val storeImages = arrayOf(R.drawable.indian_food, R.drawable.mexican_food, R.drawable.sweets_indian)
    private val storeNames = arrayOf("Saravana Bavan","Mexicano Griller","Ganga Sweets")
    private val storeTypes = arrayOf("Indian Cuisine","Mexican Cuisine","Sweets and snacks")
    private val fbAppLinks = arrayOf ("456194091431336", "660192560708681", "482077931816013")
    private val fbWebpageLinks = arrayOf ("https://www.facebook.com/sresaravanabavan",
                    "https://www.facebook.com/mexicanogriller",
                    "https://www.facebook.com/gangasweets")
    /**
     * An array of sample (order) items.
     */
    val STORE_IMAGE: MutableList<StoreImage> = ArrayList()
    val STORE_NAME: MutableList<StoreName> = ArrayList()
    val STORE_TYPE: MutableList<StoreType> = ArrayList()
    val FB_APP_LINK: MutableList<FacebookAppLink> = ArrayList()
    val FB_WEB_LINK: MutableList<FacebookWebLink> = ArrayList()

    /**
     * A map of sample (order) items, by ID.
     */
    val STORE_IMAGE_MAP: MutableMap<String, StoreImage> = HashMap()
    val STORE_NAME_MAP: MutableMap<String, StoreName> = HashMap()
    val STORE_TYPE_MAP: MutableMap<String, StoreType> = HashMap()
    val FB_APP_LINK_MAP: MutableMap<String, FacebookAppLink> = HashMap()
    val FB_WEB_LINK_MAP: MutableMap<String, FacebookWebLink> = HashMap()


    private val COUNT = 2


    init {
        // Add items.
        for (i in 0..COUNT) {
            addStoreImage(createStoreImage(i))
            addStoreName(createStoreName(i))
            addStoreType(createStoreType(i))
            addFacebookAppLink(createFacebookAppLink(i))
            addFacebookWebLink(createFacebookWebLink(i))
        }
    }

    //add data to each list item click
    private fun addStoreImage(item: StoreImage) {
        STORE_IMAGE.add(item)
        STORE_IMAGE_MAP[item.storeImage.toString()] = item
    }

    private fun addStoreName(item: StoreName) {
        STORE_NAME.add(item)
        STORE_NAME_MAP[item.storeName] = item
    }

    private fun addStoreType(item: StoreType) {
        STORE_TYPE.add(item)
        STORE_TYPE_MAP[item.storeType] = item
    }

    private fun addFacebookAppLink(item: FacebookAppLink) {
        FB_APP_LINK.add(item)
        FB_APP_LINK_MAP[item.fbAppLink] = item
    }

    private fun addFacebookWebLink(item: FacebookWebLink) {
        FB_WEB_LINK.add(item)
        FB_WEB_LINK_MAP[item.fbWebLink] = item
    }

    private fun createStoreImage(position: Int): StoreImage {
        return StoreImage(storeImages[position], makeDetails(position))
    }
    private fun createStoreName(position: Int): StoreName {
        return StoreName(storeNames[position], makeDetails(position))
    }
    private fun createStoreType(position: Int): StoreType {
        return StoreType(storeTypes[position], makeDetails(position))
    }

    private fun createFacebookAppLink(position: Int): FacebookAppLink {
        return FacebookAppLink(fbAppLinks[position], makeDetails(position))
    }

    private fun createFacebookWebLink(position: Int): FacebookWebLink {
        return FacebookWebLink(fbWebpageLinks[position], makeDetails(position))
    }


    private fun makeDetails(position: Int): String {
        val builder = StringBuilder()
        builder.append("Details about Item: ").append(position)
        for (i in 0 until position-1) {
            builder.append("\nMore details information here.")
        }
        return builder.toString()
    }

    /**
     * A List of store and store details.
     */

    data class StoreImage(val storeImage: Int, val details: String) {
        fun toInt(): Int = storeImage
    }

    data class StoreName(val storeName: String,val details: String) {
        override fun toString(): String = storeName
    }

    data class StoreType(val storeType: String,val details: String) {
        override fun toString(): String = storeType
    }

    data class FacebookAppLink(val fbAppLink: String, val details: String) {
        override fun toString(): String = fbAppLink
    }

    data class FacebookWebLink(val fbWebLink: String, val details: String) {
        override fun toString(): String = fbWebLink
    }


}
