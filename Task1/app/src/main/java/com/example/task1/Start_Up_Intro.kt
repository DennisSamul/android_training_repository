package com.example.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.LinearLayout
import androidx.viewpager.widget.ViewPager



class Start_Up_Intro : AppCompatActivity() {

    //syntax to define the Layouts
    private lateinit var viewPager: ViewPager
    private lateinit var dotLayout: LinearLayout

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        supportActionBar?.hide()
        setContentView(R.layout.start_up)

        //way to assign the layouts to a variable
        viewPager = findViewById<View>(R.id.viewPager) as ViewPager

        //calling the slide class
        val adapter = ViewPagerAdapter(this)
        viewPager.adapter = adapter

    }

    //function to got to Check in screen from slide
    fun checkIn(v:View) {
        val intent = Intent(this@Start_Up_Intro, CheckIn::class.java)
        startActivity(intent)
    }
}
