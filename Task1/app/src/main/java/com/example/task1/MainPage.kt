package com.example.task1

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import com.google.android.material.bottomnavigation.BottomNavigationView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.findNavController
import com.example.task1.order.StoreList
import com.google.android.material.navigation.NavigationView

//second call is required for the slide menu NavigationView.OnNavigationItemSelectedListener
//KotlinListPageName.OnListFragmentInteractionListener "is required for Fragments List

class MainPage : AppCompatActivity(),
    NavigationView.OnNavigationItemSelectedListener,
    MainPageList.OnListFragmentInteractionListener,
    Notification.OnFragmentInteractionListener,
    RestaurentView.OnFragmentInteractionListener{

    private val fbApp = Bundle()

    override fun onFragmentInteraction(uri: Uri) {
        Log.i("content", "param1")
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onListFragmentInteraction(item: StoreList.StoreImage?) {
        val args = Bundle()
//        args.putString("param1","Selected")
        args.putInt("param2", item!!.toInt())
        Log.i("param 2 inside list frg", args.toString())
        findNavController(R.id.nav_host).navigate(R.id.restaurentView,args)

        Log.i("merged", fbApp.toString())
    }

    //layout declaration for side menu --- will function only if the second class is provided with the activity class
    private val drawerLayout by lazy {
        findViewById<DrawerLayout>(R.id.drawer_layout)
    }


    private val onNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_order -> {
                findNavController(R.id.nav_host).navigate(R.id.main_dest)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_notification -> {
                findNavController(R.id.nav_host).navigate(R.id.notification)
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_profile -> {
                findNavController(R.id.nav_host).navigate(R.id.profile)
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_nav)

        //calls the bottom navigation panel
        val navView: BottomNavigationView = findViewById(R.id.nav_view)
        supportActionBar?.hide()
        title = ""
        navView.setOnNavigationItemSelectedListener(onNavigationItemSelectedListener)

        //below two lines is used to declare the side navigation panel
        val slideView = findViewById<NavigationView>(R.id.slide_view)
        slideView.setNavigationItemSelectedListener (this)

        Log.i("oncreate", fbApp.toString())


    }

    //below functions are used for side navigation button clickable
    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.navigation_contactus -> contactUs()
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun contactUs(): Boolean {
        drawerLayout.closeDrawer(GravityCompat.END)
        val intent = Intent(this@MainPage, ContactUs::class.java)
        startActivity(intent)
        return true
    }

    //Facebook

//    fun FacebookWebLink (v:View) {
//
//        val intent = Intent(openFacebook(this))
//        startActivity(intent)
//    }

    private fun openFacebook(context: Context): Intent {
        try {
            Log.i("content inside view", fbApp.toString())
            context.packageManager.getPackageInfo("com.facebook.katana",0)
            return Intent(Intent.ACTION_VIEW,Uri.parse("fb://page/376227335860239"))
        } catch (e:Exception){
            return Intent(Intent.ACTION_VIEW,Uri.parse("$fbApp"))
        }
    }

}
