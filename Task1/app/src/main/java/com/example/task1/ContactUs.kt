package com.example.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem

class ContactUs : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_us)
        supportActionBar?.show()
        title = "Contact Us"
    }
    // Following three functions is to make the menu items clickable
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.navigation_contactus -> contactUs()
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun contactUs(): Boolean {
        val intent = Intent(this@ContactUs, ContactUs::class.java)
        startActivity(intent)
        return true
    }
}
