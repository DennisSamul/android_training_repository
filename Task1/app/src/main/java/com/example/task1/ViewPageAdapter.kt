package com.example.task1

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import androidx.viewpager.widget.PagerAdapter
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.viewpager.widget.ViewPager

//this class is used for creating the slide
class ViewPagerAdapter(private val context: Context):PagerAdapter () {

    private var layoutInflater: LayoutInflater?= null

    //declare the store_name to be shown in each variable
    private val images = arrayOf(R.drawable.image1,R.drawable.image2,R.drawable.image3)
    private val slideHeading = arrayOf("Welcome to Window Cook", "Health is Wealth", "Become a Chef")
    private val slideContent = arrayOf("We connect individuals who love being in the kitchen as much as enjoying a neighborhood meal.",
        "Explore various dishes from our wide variety of cuisines and chefs. You can choose from quick meals or combos to big meals, healthy and controlled food and also go for monthly subscriptions.",
        "Become a favorite chef of your neighborhood and earn friends, family and money while enjoying the neighborhood meal")
    private val dots = arrayOf(R.drawable.radio1,R.drawable.radio1,R.drawable.radio1)

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view == `object`
    }

    override fun getCount(): Int {
        return slideHeading.size
    }

    //slide creating function
    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        val v = layoutInflater!!.inflate(R.layout.slide_intro,null)
        val image = v.findViewById<View>(R.id.image_view) as ImageView
        val head = v.findViewById<View>(R.id.head_view) as TextView
        val content = v.findViewById<View>(R.id.content_view) as TextView

        image.setImageResource(images[position])
        head.text = slideHeading[position]
        content.text = slideContent[position]

        if (position == 0) {
            Log.i("position 1", position.toString())
            val dot = v.findViewById<View>(R.id.dot0) as ImageView
            dot.setImageResource(dots[position])
        } else if (position == 1) {
            Log.i("position 2", position.toString())
            val dot = v.findViewById<View>(R.id.dot1) as ImageView
            dot.setImageResource(dots[position])
        } else if (position == 2) {
            Log.i("position 3", position.toString())
            val dot = v.findViewById<View>(R.id.dot2) as ImageView
            dot.setImageResource(dots[position])
        }

        val vp = container as ViewPager
        vp.addView(v, 0)

        return v
    }

    //stops the code after reaching the last slide
    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        layoutInflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        var v = layoutInflater!!.inflate(R.layout.slide_intro,null)
        val vp = container as ViewPager
//        val v = `object` as ViewPager

        if (position>slideHeading.size) {
            vp.removeView(v)
        } else {
            vp.addView(v, 0)
        }
    }
}