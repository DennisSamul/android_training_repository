package com.example.task1

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button

class CheckIn : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_check_in)
        supportActionBar?.show()
        title = ""
    }
    fun signIn(v: View) {
        val page = (v as Button).text
        val intent = Intent(this@CheckIn, SignIn::class.java)
        intent.putExtra("page",page)
        startActivity(intent)
    }
}
